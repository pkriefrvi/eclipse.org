/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { transformSnakeToCamel } from 'eclipsefdn-solstice-assets/js/api/utils';

const projectApiBaseUrl = "https://projects.eclipse.org/api";

export const getProject = async projectId => {
  try {
    const response = await fetch(`${projectApiBaseUrl}/projects/${projectId}.json`);
    const data = await response.json();
    if (data.length === 0) throw new Error('No project found.');

    const project = transformSnakeToCamel(data[0]);

    return [project, null];
  } catch (error) {
    console.error(error);

    return [null, error];
  }
}

