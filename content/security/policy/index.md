---
title: Eclipse Foundation Security Policy
seo_title: Vulnerability Reporting Policy | Eclipse Foundation
keywords: [eclipse, project, security]
tags: [eclipse, project, security]
---

Version 1.2 November 20/2024

## Overview

The purpose of the Eclipse Foundation Security Policy is to set forth the
general principles under which the Eclipse Foundation manages the reporting,
management, discussion, and disclosure of Vulnerabilities discovered in Eclipse
software. This Security Policy applies to all software developed and
distributed by the Eclipse Foundation, including all software authored by
Eclipse Committers and third-parties. This Eclipse Security Policy should at
all times be interpreted in a manner that is consistent with the Purposes of
the Eclipse Foundation as set forth in the
[Eclipse Foundation Bylaws](/org/documents/eclipse_foundation-bylaws.pdf) and
the [Eclipse Foundation Development Process](/org/projects/dev_process/).

## Terms

- *Eclipse Foundation Security Team*\
The "Eclipse Foundation Security Team" or "EF Security Team" is a part of EMO
tasked with security and Vulnerability coordination and management on behalf of
the Eclipse community.
- *Project’s Security Team*\
A "Project’s Security Team" is the team from a particular Project tasked with
security and Vulnerability management for that Project.
- *Vulnerability*\
This policy uses the ISO 27005 definition of Vulnerability: "A weakness of an
asset or group of assets that can be exploited by one or more threats."

Other terms used in this document are defined in the
[Eclipse Foundation Development Process](/projects/dev_process/).

## Eclipse Foundation Security Team

The Eclipse Foundation Security Team is the first line of defense: they triage
reports, redirect them to the appropriate Project’s Security team or the EMO
team, provide assistance and guidance in the resolution.

The EF Security Team is composed of a small number of security experts and is a
part of EMO. The EF Security Team does not resolve Vulnerabilities;
Vulnerabilities are addressed and resolved by Project’s Security Team and
Committers with guidance and assistance from the EF Security Team.

## Project's Security Team

The Project’s Security Team is responsible for coordinating the resolution of
Vulnerabilities within the Project. By default, the Project’s Security Team
includes all Committers. However, the Project may choose a different
arrangement and establish specific criteria for team nominations

The Project’s Security Team might require guidance and help from the EF
Security Team, notably in cases when a synchronization between multiple
Projects' Security Teams is necessary.

The EMO will, upon request from a Project Security Team’s, allocate a
confidential channel for discussing undisclosed Vulnerabilities. Confidential
channels must only be used for communication on details of a resolution of an
undisclosed Vulnerability. Discussions about disclosed Vulnerabilities should
take place on public channels.

The Project’s Security Team is closely collaborating with the Eclipse
Foundation Security Team. Therefore, the Project’s Security Team shall
immediately communicate their preferred method of communication to the Eclipse
Foundation Security Team upon creation and keep them updated on any changes.

## Discussion

The Eclipse Foundation is responsible for establishing communication channels
for the Eclipse Foundation Security Team and for Projects' Security Teams, as
appropriate.

Every potential issue reported through established communication channels
should be triaged, and relevant parties notified. Initial discussions of a
potential Vulnerability may occur privately between the Project’s Security
Team and the EF Security Team. Once confirmed, the discussion should be moved
to and tracked by an Eclipse Foundation-supported issue tracker as early as
possible to allow the mitigation process to proceed. Appropriate effort must be
made to ensure the visibility and legitimacy of every reported issue from the
outset.

If there is a difference of opinion between the individual reporting the
potential issue, the Project’s Security Team, and the EF Security Team
regarding the classification of the issue as a Vulnerability, the Eclipse
Foundation Security Team will provide the final classification.

## Resolution

A Vulnerability is considered resolved when a fix or workaround is available,
or when it is determined that a fix is not possible or desirable. If no fix or
workaround is available, a risk analysis must be provided. All Vulnerabilities
must be resolved.

It is at the discretion of the Project’s Security Team and Project Leadership
Chain to determine what subset of the committers are best suited to resolve
Vulnerabilities. The Project’s Security Team may also--at their
discretion--assemble external resources (e.g. subject matter experts) or seek
the expertise of the EF Security Team.

In the unlikely event that a project team does not engage in good faith to
resolve a Vulnerability, an member of EMO may--at their discretion--initiate
the Grievance Process as defined by the
[Eclipse Foundation Development Process](/projects/dev_process/).

## Distribution

Once a Vulnerability is resolved, the updated software must be made available
to the community. This updated software should be either a regular or a service
release.

At a minimum, updated software must be made available via normal project
distribution channels.

## Disclosure

The information on the Vulnerability is initially limited to the reporter,
Project’s Security Team, and EF Security Team, but may be expanded to include
other individuals.

It is strongly recommended that public disclosure occurs immediately after a
bugfix release is available. In all cases, users and administrators of Eclipse
Projects software must be informed of any Vulnerability so they can assess the
risk and take appropriate action to protect their users, servers, and systems
from potential exploits.

### Timing

The timing of disclosure is left to the discretion of the Project Leadership
Chain. In the absence of specific guidance from the Project Leadership Chain,
the following guidelines are recommended:

- Resolved Vulnerabilities are disclosed immediately after a release containing
those fixes is available;
- Projects are expected to resolve a Vulnerability in no more than three
months; this delay might be extended by the Project Leadership Chain together
with the Eclipse Foundation Security team if appropriate;
- The Project Leadership Chain with the guidance of the Eclipse Foundation
Security Team might disclose earlier if necessary;

All Vulnerabilities must be eventually disclosed.

### Quiet Disclosure

A Vulnerability may be *quietly* disclosed by simply removing visibility
restrictions and documenting a risk analysis or providing the reasoning for not
considering it a Vulnerability.

In general, quiet disclosure is appropriate only for issues that are identified
by a Project’s Security Team as having been erroneously marked as
Vulnerabilities or Vulnerabilities with low severity and when exploitation is
highly unlikely.

### Progressive Disclosure

Knowledge of a Vulnerability can be extended to specific individuals before it
is reported to the community. A Vulnerability may--at the discretion of the
Project’s Security Team--be disclosed to specific individuals. Project’s
Security Team may, for example, provide access to a subject-matter expert to
solicit help or advice.

### Full Disclosure

All Vulnerabilities must eventually be fully disclosed to the community at
large.

To complete the disclosure of a Vulnerability, all restrictions on visibility
must be removed and the Vulnerability reported via channels provided by the
Eclipse Foundation.

### Reporting

A Project’s Security Team may, at their discretion, opt to disclose a
Vulnerability to a reporting authority.

The EF Security Team will determine how to engage with Vulnerability reporting
authorities.

## History

Changes made in this document:

### Change Log

#### [2024] - 2024-11-20 (version 1.2)

- Changed back the name to "Security Policy".
- Introduced the concept of Project Security Team and its relationship to the
Eclipse Foundation Security Team
- Elaborated on the responsibilities on both teams in the Discussion,
Resolution, Distribution, and Disclosure section.

#### [2019] - 2019-03-06 (version 1.1)

##### Changes

- Changed the name from "Security Policy" to "Vulnerability Reporting Policy"
- Formalized terms into their own section.
- Changed several occurances of the word "can" to "may" to improve clarity.

##### Added

- Added a pointer to the Grievance Handling section of the Eclipse Foundation Development Process.

##### Removed

- Removed references to specific technology (e.g., Bugzilla or specific mailing
  lists). These are implementation details.
- Removed references to the Eclipse Planning Council and Simultaneous Release.
