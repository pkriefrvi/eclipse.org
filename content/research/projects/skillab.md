---
title: "SKILLAB"
date: 2024-01-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/skillab.png"
tags: ["Life long learning", "Educational policy", "Vocational training", "Labor Market Analytics", "Digital Skills", "Skill Shortages", "Competency Management", "Machine Learning", "Data Mining/Analytics", "Forecasting", "Human Resource Management", "Digital Policies", "Recommendation Systems"]
homepage: "https://skillab-project.eu/"
facebook: ""
linkedin: "https://www.linkedin.com/company/skillab-project"
twitter: "https://twitter.com/SkillabProject"
youtube: ""
funding_bodies: ["horizonEU"]
eclipse_projects: []
project_topic: "Skills, Education"
summary: "Monitoring The Demand And Supply Of Skills In The European Labour Market"
---

The proposed project aims at:
- (a) identifying skills shortages and gaps in the labor market for different regions, sectors,time periods 
- (b) supporting all kinds of enterprises in developing their human resource strategy, including both training of current employees, and skill shaping of future employees, 
- (c) supporting recruiting enterprises looking for specific skills, 
- (d) supporting EU citizens in developing their skills in an informed way and eventually finding a suitable job and
- (e) aiming to find good candidates for emerging roles, but also to equip existing employees with the necessary skills to retain their jobs, thus improving job
retainability and reducing employee churn.

The project will meet its goals by designing and implementing an intelligent platform for managing labor market skills and skill gaps.
The system will monitor and mine internet resources and EU initiatives to acquire and process meaningful raw data about existing
and sought skills, and entities offering and seeking jobs. State-of-the-art IT technologies will be used to empower the platform and
achieve its goals, including advanced statistical and network and data analysis, machine learning and competency mining. On the basis of
such analyses, recommendation tools for HRM, educational profile upskilling and digital policies will be made available to users. The
system will offer its findings in an easy, human-centric with SSH principles, ready to use, and ethically correct form to both enterprises
and individuals.

Such objectives are highly relevant to the objectives of the call. In particular, it is expected that the project will
contribute to the multi-faceted assessment of the labor market status through its data acquiring and analysis tools. Continuous
monitoring will allow for detecting current and future trends and gaps in requested and processed skills. The platform will offer a
knowledge base for recommendation tools to develop skill strategies both for enterprises and individuals.


This project is running from January 2024 - December 2026.

---

## Consortium
* ARISTOTELIO PANEPISTIMIO THESSALONIKIS (Coordinator) - Greece
2 UNIVERSITY OF MACEDONIA - Greece
3 DIETHNES PANEPISTIMIO ELLADOS - Greece
4 ETHNIKO KENTRO EREVNAS KAI TECHNOLOGIKIS ANAPTY - Greece
5 ASOCIATIA CLUJ IT - Romania
6 NETCOMPANY-INTRASOFT SA - Luxembourg
7 ECLIPSE FOUNDATION EUROPE GMBH - Germany
8 TELEFONICA INVESTIGACION Y DESARROLLO SA - Spain
9 HUMBOLDT-UNIVERSITAET ZU BERLIN - Germany
10 AGENTIA JUDETEANA PENTRU OCUPAREA FORTEI DE MUN - Romania