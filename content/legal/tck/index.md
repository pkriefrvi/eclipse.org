---
title: Eclipse Foundation Technology Compatibility Kit License
author: Eclipse Foundation
keywords:
    - legal
    - Technology Compatibility Kit License
---

**Do not modify the text of this license.**

Specification project committers: include this text _verbatim_ with your TCK.

An AsciiDoc version of this file is available [here](/legal/adoc/eftl-1.1.adoc). Plain HTML is [here](tck.html).

The Eclipse Foundation Specification License is [here](/legal/efsl/).

&darr; The actual licence text starts below this line. &darr;

***

{{< pages/legal/inject-content >}}
