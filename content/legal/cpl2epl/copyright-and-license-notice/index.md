---
title: Default Eclipse Foundation Copyright and License Notice
author: Mike Milinkovich
is_deprecated: true
keywords:
  - legal
  - documents
  - cpl
  - epl
---

Last updated September 8th, 2004 (see [the Eclipse Project Handbook](/projects/handbook/#ip-copyright-headers) for current information)

With the Eclipse Foundation transitioning from the CPL to the EPL (for further information see the [CPL to EPL Transition Plan](/legal/cpl2epl/cpl2epl-transition-plan.pdf) (.pdf) and matching [FAQ](/legal/cpl2epl/faq/)), the default license for a project will either be the CPL or the EPL. For CPL projects, the notice template is:

/\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*  
 \* Copyright (c) {DATE} {INITIAL COPYRIGHT OWNER} {OTHER COPYRIGHT OWNERS}.  
 \* All rights reserved. This program and the accompanying materials  
 \* are made available under the terms of the Common Public License v1.0  
 \* which accompanies this distribution, and is available at  
 \* http://www.eclipse.org/legal/cpl-v10.html  
 \*  
 \* Contributors:  
 \*    {INITIAL AUTHOR} - initial API and implementation  
 \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*/

For EPL projects, the notice template is:

/\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*  
 \* Copyright (c) {DATE} {INITIAL COPYRIGHT OWNER} {OTHER COPYRIGHT OWNERS}.  
 \* All rights reserved. This program and the accompanying materials  
 \* are made available under the terms of the Eclipse Public License v1.0  
 \* which accompanies this distribution, and is available at  
 \* http://www.eclipse.org/legal/epl-v10.html  
 \*  
 \* Contributors:  
 \*    {INITIAL AUTHOR} - initial API and implementation  
 \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*/

NOTE: While the CPL to EPL Transition Plan is in effect, do NOT use the EPL notice unless you know for certain that your project has transitioned fully to the EPL.

If the contents of the file are to be governed by another license, different text will be required. Contact the relevant PMC for assistance with this.

**{DATE}** is either a year or a range of years with the first and last years of the range separated by a comma. So for example: "2004" or "2000, 2004". The first year is when the contents of the file were first created and the last year is the last year is when the contents were last modified.

**{INITIAL COPYRIGHT OWNER}** is the copyright owner that created the initial content. If the content is subsequently modified and appended to by other copyright owners, the words "and others" are typically appended. So for example: "XYZ Corp." or "XYZ Corp. and others". The words "and others" are used to avoid having to list every copyright owner and because often, most of the content in the file was contributed the by initial copyright owner with subsequent modifications by others being smaller. However especially if the number of copyright owners is small (e.g. two), there is nothing wrong with listing all of them especially if their contributions are more proportionately equal. For example: "XYZ Corp., John Smith, and ABC Enterprises."

NOTE: Do not confuse copyright owners with authors (which is explained below). A copyright owner may be either an individual or an entity. If a person is making their contributions on behalf of their employer, then the copyright owner is usually the employer (i.e. "XYZ Corp." and not "John Smith").

**{INITIAL AUTHOR}** is the name of the person that wrote the initial content. Subsequent authors are listed on proceding lines with a short description of the contribution. If there is an associated Bugzilla report number, then it is good practice to include it here. Over time, this section actually becomes a log of the contributions. Although it could become quite lengthy in some cases, it should be maintained for two important reasons - to acknowledge the contributions of individuals (especially when their contribution is owned by their employer), and to assist with tracking the pedigree of the code (who wrote what). It is good practice to list the name of the author with their employer if the latter owns the copyright. For example: "John Smith (XYZ Corp.)".
