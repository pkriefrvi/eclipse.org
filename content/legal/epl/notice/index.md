---
title: Eclipse Foundation Software User Agreement
author: Mike Milinkovich
hide_page_title: true
keywords:
  - eclipse
  - license
  - user
  - agreement
  - sua
related:
  links:
    - text: Software User Agreement in plain HTML
      url: notice.html
    - text: Software User Agreement in Java properties format
      url: feature.properties.txt
    - text: Guide to legal documents
      url: http://www.eclipse.org/projects/handbook/#legaldoc
---

{{< pages/legal/inject-content >}}
