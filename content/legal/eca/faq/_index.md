---
title: Eclipse Contributor Agreement (ECA) FAQ
author: Mike Milinkovich
keywords:
  - legal
  - documents
  - about
  - contributor
  - license
  - agreement
  - IP
  - intellectual property
  - ECA
---

{{< pages/legal/eca-faq >}}
