---
# This page is not meant to be accessible by users.
# It has been given a temporary redirect to /org/value/ 
# in the nginx config.
title: "Open Source for Business"
metatag_robots: "noindex, nofollow"
layout: "single"
---
